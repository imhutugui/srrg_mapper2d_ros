#pragma once

#include "graph_occupancy_grid.h"

// ROS includes
#include <ros/ros.h>
#include <nav_msgs/OccupancyGrid.h>
#include <nav_msgs/GetMap.h>

class OccupancyGridServer {

 public:
  OccupancyGridServer(GraphOccupancyGrid* graph_occ_grid);

  inline void setMapFrame(std::string map_frame) {_map_frame = map_frame;}
  inline std::string mapFrame() const {return _map_frame;}
  inline void setMapTopic(std::string map_topic) {_map_topic = map_topic;}
  inline std::string mapTopic() const {return _map_topic;}

  void init();
  void publish();
  
 protected:

  GraphOccupancyGrid* _graph_occ_grid;

  std::string _map_frame;
  std::string _map_topic;

  ros::NodeHandle _nh;
  ros::Publisher _pub_map;
  ros::Publisher _pub_map_metadata;
  ros::ServiceServer _map_server;

  void createMapMessage(nav_msgs::OccupancyGrid &map_msg);
  bool mapCallback(nav_msgs::GetMap::Request &req, nav_msgs::GetMap::Response &res);

};
