#pragma once

//g2o includes
#include "g2o/core/optimizable_graph.h"
#include "g2o/types/slam2d/vertex_se2.h"
#include "g2o/types/data/robot_laser.h"

// ROS includes
#include <ros/ros.h>
#include <tf/tf.h>
#include <tf/transform_broadcaster.h>
#include <sensor_msgs/PointCloud.h>
#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/Point32.h>

// SRRG includes
#include "srrg_types/types.hpp"

#include <thread>

using namespace g2o;
using namespace srrg_core;

class GraphROSPublisher {
 public:
  GraphROSPublisher(OptimizableGraph *graph);

  inline void setMapFrame(std::string map_frame) {_map_frame = map_frame;}
  inline std::string mapFrame() const {return _map_frame;}
  inline void setOdomFrame(std::string odom_frame) {_odom_frame = odom_frame;}
  inline std::string odomFrame() const {return _odom_frame;}

  inline void setRate(int rate) {_rate = rate;}
  inline int rate() {return _rate;}

  inline void setEstimate(const Eigen::Isometry2f& estimate) {_estimate = estimate;}
  inline Eigen::Isometry2f& estimate() {return _estimate;}
  inline void setOdom(const Eigen::Isometry2f& odom) {_odom = odom;}
  inline Eigen::Isometry2f& odom() {return _odom;}
  
  void start(bool publish_graph, bool publish_tf);
  void stop();
  void publishTF();
  void publishGraph();
  
 protected:

  OptimizableGraph *_graph;
  
  std::string _map_frame;
  std::string _odom_frame;

  int _rate; //ms

  Eigen::Isometry2f _estimate;
  Eigen::Isometry2f _odom;
  
  // Tf publishing management
  bool _publish_tf;
  tf::TransformBroadcaster _tf_broadcaster;
  std::thread _tf_publish_thread;

  // Graph publishing
  bool _publish_graph;
  ros::NodeHandle _nh;
  ros::Publisher _pub_trajectory;
  ros::Publisher _pub_lasermap;
  
  //mutex
  
};
