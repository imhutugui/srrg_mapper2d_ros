#include "message_handler.h"

MessageHandler::MessageHandler(tf::TransformListener* listener_) {
  _listener = listener_;
 
  _laser_offset.setIdentity();
}

MessageHandler::~MessageHandler() {
}
/*
void MessageHandler::laser_callback(const sensor_msgs::LaserScan::ConstPtr& msg) {
  ++_frame_count;

  if (_frame_count % _frame_skip != 0)
    return;

  if (_projector == NULL) {
    float fov = 0.f;
    fov = msg->angle_increment * (msg->ranges.size()-1);
    _projector = new Projector2D();
    _projector->setMaxRange(msg->range_max);
    _projector->setMinRange(msg->range_min);
    _projector->setFov(fov);
    std::cerr << " ranges size" << msg->ranges.size() << endl;
    std::cerr << " maxangle" << msg->angle_max *180/M_PI << endl;
    std::cerr << " minangle" << msg->angle_min *180/M_PI << endl;
    std::cerr << " angleinc" << msg->angle_increment *180/M_PI << endl;

    _projector->setNumRanges(msg->ranges.size());
  }

  Eigen::Isometry2f guess;
  guess.setIdentity();
  if (_last_imu_msg.header.stamp.toSec()>0) {
    guess.translation().setZero();
    float angle=Eigen::AngleAxisf(Eigen::Quaternionf(_last_imu_msg.orientation.x,
						     _last_imu_msg.orientation.y,
						     _last_imu_msg.orientation.z,
						     _last_imu_msg.orientation.w).toRotationMatrix()).angle();
    guess.linear()=Eigen::Rotation2Df(angle).toRotationMatrix();
  }
  CloudWithTime* current = new CloudWithTime(msg->header.stamp, guess);
  _projector->unproject(*current, msg->ranges);
  _clouds.push_back(current);
}*/

void MessageHandler::odom_laser_callback(const nav_msgs::Odometry::ConstPtr& odommsg, const sensor_msgs::LaserScan::ConstPtr& scanmsg) {

  Eigen::Isometry3f odom_pose;
  odom_pose.translation() = Eigen::Vector3f(odommsg->pose.pose.position.x,
					    odommsg->pose.pose.position.y,
					    odommsg->pose.pose.position.z);
  Eigen::Quaternionf pose_q;
  pose_q.x() = odommsg->pose.pose.orientation.x;
  pose_q.y() = odommsg->pose.pose.orientation.y;
  pose_q.z() = odommsg->pose.pose.orientation.z;
  pose_q.w() = odommsg->pose.pose.orientation.w;
  odom_pose.linear()=pose_q.toRotationMatrix();

  //Getting laser offset
  Eigen::Isometry3f offset3f = Eigen::Isometry3f::Identity();
  tf::StampedTransform transform;
  if (_listener){
    try{
      _listener->waitForTransform(_base_link_frame_id, scanmsg->header.frame_id, 
				  scanmsg->header.stamp, 
				  ros::Duration(0.1) );
      _listener->lookupTransform (_base_link_frame_id, scanmsg->header.frame_id, 
				  scanmsg->header.stamp, 
				  transform);
    }
    catch (tf::TransformException ex){
      ROS_ERROR("%s",ex.what());
    }
    Eigen::Quaternionf q;
    q.x() = transform.getRotation().x();
    q.y() = transform.getRotation().y();
    q.z() = transform.getRotation().z();
    q.w() = transform.getRotation().w();
    _laser_offset.setIdentity();
    _laser_offset.translation() = Eigen::Vector2f(transform.getOrigin().x(),
						  transform.getOrigin().y());
    Eigen::Matrix3f offset_rotation = q.toRotationMatrix();
    _laser_offset.linear() = offset_rotation.block<2,2>(0,0);

    offset3f.linear() = offset_rotation;
    offset3f.translation() = Eigen::Vector3f(transform.getOrigin().x(),
					     transform.getOrigin().y(), 
					     transform.getOrigin().z());
  }


  //Transforming scan into a LaserMessage
  LaserMessage* laser=new LaserMessage(_laser_topic, scanmsg->header.frame_id,  scanmsg->header.seq, scanmsg->header.stamp.toSec());

  laser->setOdometry(odom_pose);
  
  laser->setOffset(offset3f);
  laser->setMinAngle(scanmsg->angle_min);
  laser->setMaxAngle(scanmsg->angle_max);
  laser->setAngleIncrement(scanmsg->angle_increment);
  laser->setMinRange(scanmsg->range_min);
  laser->setMaxRange(scanmsg->range_max);
  laser->setTimeIncrement(scanmsg->time_increment);
  laser->setScanTime(scanmsg->scan_time);

  std::vector<float> ranges;
  ranges.resize(scanmsg->ranges.size());
  for (size_t i = 0; i < scanmsg->ranges.size(); i++){
    if ((scanmsg->ranges[i] < scanmsg->range_min) || std::isnan(scanmsg->ranges[i]))
      ranges[i]=0;
    else if (scanmsg->ranges[i] > scanmsg->range_max)
      ranges[i]=scanmsg->range_max;
    else
      ranges[i]=scanmsg->ranges[i];
  }
  laser->setRanges(ranges);

  std::vector<float> intensities;
  intensities.resize(scanmsg->intensities.size());
  for (size_t i = 0; i < scanmsg->intensities.size(); i++)
    intensities[i]=scanmsg->intensities[i];
  laser->setIntensities(intensities);

  _laser_msgs.push_back(laser);
}
