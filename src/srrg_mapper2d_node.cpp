#include <fstream>
#include <sstream>

// ROS
#include <ros/ros.h>

#include <sensor_msgs/LaserScan.h>
#include <nav_msgs/Odometry.h>
#include <tf/transform_broadcaster.h>

//Qt
#include <QApplication>
#include <QHBoxLayout>

#include "tracker_viewer.h"
#include "simple_graph_viewer.h"
#include "mapper2d.h"
#include "laser_message_tracker.h"
#include "laser_message_tracker_params.h"
#include "mapper2d_params.h"

#include "message_handler.h"
#include "g2o2ros/occupancy_grid_server.h"
#include "g2o2ros/graph_ros_publisher.h"

using namespace srrg_scan_matcher_gui;
using namespace srrg_mapper2d;
using namespace srrg_mapper2d_gui;
using namespace srrg_scan_matcher;
using namespace srrg_core;
using namespace g2o;

// ugly program parameters

std::string laser_topic;
std::string odom_topic;
std::string odom_frame;
std::string base_link_frame_id;
std::string map_topic;
std::string map_frame;

bool use_gui = false;
bool draw_ellipse_data = false;
bool save_clouds = false;
bool publish_map = false;
bool publish_graph = false;
bool publish_tf = false;

string outgraphFilename = "out.g2o";
MessageHandler* message_handler;


LaserMessageTrackerParams* trackerparams = new LaserMessageTrackerParams;
Mapper2DParams* mapperparams = new Mapper2DParams;

void readParameters(){
  ros::NodeHandle private_nh("~");
  bool verbose;
  bool log_times;
  double bpr;
  int iterations;
  double inlier_distance;
  double min_correspondences_ratio;
  double local_map_clipping_range;
  double local_map_clipping_translation_threshold;
  double voxelize_res;
  double merging_distance, merging_normal_angle;
  bool projective_merge;
  double tracker_damping;
  double max_matching_range;
  double min_matching_range;
  int num_matching_beams;
  double matching_fov;
  int frame_skip;
  double laser_translation_threshold;
  double laser_rotation_threshold;
  int odom_weights_x, odom_weights_y, odom_weights_theta;
  std::string cfinder;

  double closures_inlier_threshold;
  int closures_window;
  int closures_min_inliers;
  double lcaligner_inliers_distance;
  double lcaligner_min_inliers_ratio;
  int lcaligner_min_num_correspondences;
  bool use_merger;
  double vertex_translation_threshold;
  double vertex_rotation_threshold;

  // ROS parameters setting
  // ROS topics
  private_nh.param("laser_topic", laser_topic, std::string("/scan"));
  private_nh.param("odom_topic", odom_topic, std::string("/odom"));
  private_nh.param("odom_frame", odom_frame, std::string("/odom"));
  private_nh.param("base_link_frame_id", base_link_frame_id, std::string("/base_link"));
  private_nh.param("map_topic", map_topic, std::string("/map"));
  private_nh.param("map_frame", map_frame, std::string("/map"));
      
  private_nh.getParam("use_gui", use_gui);
  private_nh.getParam("draw_ellipse_data", draw_ellipse_data);
  if (private_nh.getParam("verbose", verbose)){
    trackerparams->setVerbose(verbose);
    mapperparams->setVerbose(verbose);
  }
  private_nh.getParam("publish_map", publish_map);
  private_nh.getParam("publish_graph", publish_graph);
  private_nh.getParam("publish_tf", publish_tf);
  if (private_nh.getParam("log_times", log_times))
    mapperparams->setLogTimes(log_times);
  private_nh.getParam("save_clouds", save_clouds);

  // Tracker params
  if (private_nh.getParam("bpr", bpr))
    trackerparams->setBpr(bpr);
  if (private_nh.getParam("iterations", iterations))
    trackerparams->setIterations(iterations);
  if (private_nh.getParam("inlier_distance", inlier_distance))
    trackerparams->setInlierDistance(inlier_distance);
  if (private_nh.getParam("min_correspondences_ratio", min_correspondences_ratio))
    trackerparams->setMinCorrespondencesRatio(min_correspondences_ratio);
  if (private_nh.getParam("local_map_clipping_range", local_map_clipping_range))
    trackerparams->setLocalMapClippingRange(local_map_clipping_range);
  if (private_nh.getParam("local_map_clipping_translation_threshold", local_map_clipping_translation_threshold))
    trackerparams->setLocalMapClippingTranslationThreshold(local_map_clipping_translation_threshold);
  if (private_nh.getParam("voxelize_res", voxelize_res))
    trackerparams->setVoxelizeResolution(voxelize_res);
  if (private_nh.getParam("merging_distance", merging_distance))
    trackerparams->setMergingDistance(merging_distance);
  if (private_nh.getParam("merging_normal_angle", merging_normal_angle))
    trackerparams->setMergingNormalAngle(merging_normal_angle);
  if (private_nh.getParam("projective_merge", projective_merge))
    trackerparams->setProjectiveMerge(projective_merge);
  if (private_nh.getParam("tracker_damping", tracker_damping))
    trackerparams->setTrackerDamping(tracker_damping);
  if (private_nh.getParam("max_matching_range", max_matching_range))
    trackerparams->setMaxMatchingRange(max_matching_range);
  if (private_nh.getParam("min_matching_range", min_matching_range))
    trackerparams->setMinMatchingRange(min_matching_range);
  if (private_nh.getParam("num_matching_beams", num_matching_beams))
    trackerparams->setNumMatchingBeams(num_matching_beams);
  if (private_nh.getParam("matching_fov", matching_fov))
    trackerparams->setMatchingFov(matching_fov);
  if (private_nh.getParam("frame_skip", frame_skip)){
    if (frame_skip <= 0)
      frame_skip = 1;
    trackerparams->setFrameSkip(frame_skip);
  }
  if (private_nh.getParam("laser_translation_threshold", laser_translation_threshold))
    trackerparams->setLaserTranslationThreshold(laser_translation_threshold);
  if (private_nh.getParam("laser_rotation_threshold", laser_rotation_threshold))
    trackerparams->setLaserRotationThreshold(laser_rotation_threshold);

  private_nh.param("odom_weights_x", odom_weights_x, 0);
  private_nh.param("odom_weights_y", odom_weights_y, 0);
  private_nh.param("odom_weights_theta", odom_weights_theta, 0);
  Eigen::Vector3i odom_weights(odom_weights_x, odom_weights_y, odom_weights_theta);
  trackerparams->setOdomWeights(odom_weights);
  
  if (private_nh.getParam("cfinder", cfinder))
    trackerparams->setCorrespondenceFinderType(cfinder);
  
  // Mapper params
  if (private_nh.getParam("closures_inlier_threshold", closures_inlier_threshold))
    mapperparams->setClosuresInlierThreshold(closures_inlier_threshold);
  if (private_nh.getParam("closures_window", closures_window))
    mapperparams->setClosuresWindow(closures_window);
  if (private_nh.getParam("closures_min_inliers", closures_min_inliers))
    mapperparams->setClosuresMinInliers(closures_min_inliers);
  if (private_nh.getParam("lcaligner_inliers_distance", lcaligner_inliers_distance))
    mapperparams->setLcalignerInliersDistance(lcaligner_inliers_distance);
  if (private_nh.getParam("lcaligner_min_inliers_ratio", lcaligner_min_inliers_ratio))
    mapperparams->setLcalignerMinInliersRatio(lcaligner_min_inliers_ratio);
  if (private_nh.getParam("lcaligner_min_num_correspondences", lcaligner_min_num_correspondences))
    mapperparams->setLcalignerMinNumCorrespondences(lcaligner_min_num_correspondences);
  if (private_nh.getParam("use_merger", use_merger))
    mapperparams->setUseMerger(use_merger);
  if (private_nh.getParam("vertex_translation_threshold", vertex_translation_threshold))
    mapperparams->setVertexTranslationThreshold(vertex_translation_threshold);
  if (private_nh.getParam("vertex_rotation_threshold", vertex_rotation_threshold))
    mapperparams->setVertexRotationThreshold(vertex_rotation_threshold);
  private_nh.getParam("o", outgraphFilename);

  std::cout << "Launched with params:" << std::endl;
  std::cout << "_laser_topic:="        << laser_topic << std::endl;
  std::cout << "_odom_topic:="         << odom_topic  << std::endl;
  std::cout << "_odom_frame:="         << odom_frame  << std::endl;
  std::cout << "_base_link_frame_id:=" << base_link_frame_id << std::endl;
  std::cout << "_map_topic:="          << map_topic << std::endl;
  std::cout << "_map_frame:="          << map_frame << std::endl;
  std::cout << "_use_gui:="              << (use_gui?"true":"false")            << std::endl;
  std::cout << "_draw_ellipse_data:="    << (draw_ellipse_data?"true":"false")  << std::endl;
  std::cout << "_verbose:="              << (trackerparams->verbose()?"true":"false") << std::endl;
  std::cout << "_publish_map:="          << (publish_map?"true":"false")            << std::endl;
  std::cout << "_publish_graph:="        << (publish_graph?"true":"false")            << std::endl;
  std::cout << "_publish_tf:="           << (publish_tf?"true":"false")            << std::endl;  
  std::cout << "_log_times:="            << (mapperparams->logTimes()?"true":"false") << std::endl;
  std::cout << "_save_clouds:="          << (save_clouds?"true":"false") << std::endl;
  std::cout << "_bpr:="                  << trackerparams->bpr()                << std::endl;
  std::cout << "_iterations:="           << trackerparams->iterations()         << std::endl;
  std::cout << "_inlier_distance:="      << trackerparams->inlierDistance()     << std::endl;
  std::cout << "_min_correspondences_ratio:=" << trackerparams->minCorrespondencesRatio() << std::endl;
  std::cout << "_local_map_clipping_range:="  << trackerparams->localMapClippingRange()   << std::endl;
  std::cout << "_local_map_clipping_translation_threshold:=" << trackerparams->localMapClippingTranslationThreshold() << std::endl;
  std::cout << "_voxelize_res:="         << trackerparams->voxelizeResolution() << std::endl;
  std::cout << "_merging_distance:="     << trackerparams->mergingDistance()    << std::endl;
  std::cout << "_merging_normal_angle:=" << trackerparams->mergingNormalAngle() << std::endl;
  std::cout << "_projective_merge:="     << (trackerparams->projectiveMerge()?"true":"false") << std::endl;
  std::cout << "_tracker_damping:="      << trackerparams->trackerDamping()   << std::endl;
  std::cout << "_max_matching_range:="   << trackerparams->maxMatchingRange() << std::endl;
  std::cout << "_min_matching_range:="   << trackerparams->minMatchingRange() << std::endl;
  std::cout << "_num_matching_beams:="   << trackerparams->numMatchingBeams() << std::endl;
  std::cout << "_matching_fov:="         << trackerparams->matchingFov()      << std::endl;
  std::cout << "_frame_skip:="           << trackerparams->frameSkip()        << std::endl;
  std::cout << "_laser_translation_threshold:=" << trackerparams->laserTranslationThreshold() << std::endl;
  std::cout << "_laser_rotation_threshold:="    << trackerparams->laserRotationThreshold()    << std::endl;
  std::cout << "_odom_weights:="         << trackerparams->odomWeights().transpose()  << std::endl;
  std::cout << "_cfinder:="              << trackerparams->correspondenceFinderType() << std::endl;

  std::cout << "_closures_inlier_threshold:="   << mapperparams->closuresInlierThreshold()   << std::endl;
  std::cout << "_closures_window:="             << mapperparams->closuresWindow()            << std::endl;
  std::cout << "_closures_min_inliers:="        << mapperparams->closuresMinInliers()        << std::endl;
  std::cout << "_lcaligner_inliers_distance:="  << mapperparams->lcalignerInliersDistance()  << std::endl;
  std::cout << "_lcaligner_min_inliers_ratio:=" << mapperparams->lcalignerMinInliersRatio()  << std::endl;
  std::cout << "_lcaligner_min_num_correspondences:=" << mapperparams->lcalignerMinNumCorrespondences() << std::endl;
  std::cout << "_use_merger:="                        << (mapperparams->useMerger()?"true":"false")     << std::endl;
  std::cout << "_vertex_translation_threshold:=" << mapperparams->vertexTranslationThreshold()     << std::endl;
  std::cout << "_vertex_rotation_threshold:="    << mapperparams->vertexRotationThreshold()        << std::endl;
  std::cout << "_o:="                            << outgraphFilename << std::endl;
    
  fflush(stdout);

}

int main(int argc, char **argv) {

  ros::init(argc, argv, "srrg_mapper2d_node");
  ros::NodeHandle n;
  readParameters();

  LaserMessageTracker* tracker = new LaserMessageTracker();
  SimpleGraph* sgraph = new SimpleGraph;
  Mapper2D* mapper = new Mapper2D(tracker, sgraph);

  trackerparams->setTracker(tracker);
  mapperparams->setMapper(mapper);

  trackerparams->applyParams();
  mapperparams->applyParams();

  //GUI set up
  QApplication* app=0;
  QWidget* mainWindow=0;
  Tracker2DViewer* tviewer=0;
  SimpleGraphViewer* sviewer=0;
  if (use_gui) {
    app=new QApplication(argc, argv);
    mainWindow = new QWidget();
    mainWindow->setWindowTitle("mapper2d_viewer");
    QHBoxLayout* hlayout = new QHBoxLayout();
    mainWindow->setLayout(hlayout);
    tviewer=new Tracker2DViewer(tracker);
    sviewer=new SimpleGraphViewer(sgraph);
    hlayout->addWidget(tviewer);
    hlayout->addWidget(sviewer);
    tviewer->init();
    mainWindow->showMaximized();
    mainWindow->show();
    tviewer->show();
    sviewer->show();
    sviewer->setDrawCovariances(draw_ellipse_data);
  }
  int prevSize = sgraph->graph()->vertices().size();
    
  tf::TransformListener *listener = new tf::TransformListener();
  message_handler = new MessageHandler(listener);
  message_handler->setBaseLinkFrame(base_link_frame_id);
  message_handler->setLaserTopic(laser_topic);

  message_filters::Subscriber<nav_msgs::Odometry> subSyncOdom(n, odom_topic, 10);
  message_filters::Subscriber<sensor_msgs::LaserScan> subSyncScan(n, laser_topic, 10);

  message_filters::Synchronizer<MyOdomScanSyncPolicy> syncOdomScan(MyOdomScanSyncPolicy(10), subSyncOdom, subSyncScan);
  syncOdomScan.registerCallback(boost::bind(&MessageHandler::odom_laser_callback, message_handler, _1, _2));

  //ROS map publisher and tf managment
  GraphOccupancyGrid* graph2occ = 0;
  OccupancyGridServer* gserver = 0;
  GraphROSPublisher* gpub = 0;

  if (publish_map) {
    graph2occ = new GraphOccupancyGrid(sgraph->graph());
    graph2occ->setMaxRange(15.0);
    graph2occ->setUsableRange(15.0);

    gserver = new OccupancyGridServer(graph2occ);
    gserver->setMapFrame(map_frame);
    gserver->setMapTopic(map_topic);
    gserver->init();
  }

  if (publish_graph || publish_tf) {
      gpub = new GraphROSPublisher(sgraph->graph());
      gpub->setMapFrame(map_frame);
      gpub->setOdomFrame(odom_frame);
      gpub->start(publish_graph, publish_tf);
  }

  bool first_msg = true;
  std::cerr << "Entering ROS loop" << std::endl;
  ros::Rate loop_rate(10);
  while (ros::ok()) {
    app->processEvents();
    std::list<LaserMessage*>& laser_msgs = message_handler->laser_msgs();
    while (laser_msgs.size() > 0) {
      LaserMessage* laser_msg = laser_msgs.front();
      if (first_msg){
	mapper->init(laser_msg);
	first_msg = false;
      } else
	mapper->compute(laser_msg);

      if (publish_tf) {
	Eigen::Isometry2f curr_odom = toIsometry2f(laser_msg->odometry());
	gpub->setOdom(curr_odom);
	Eigen::Isometry2f curr_est = mapper->estimate();
	gpub->setEstimate(curr_est);
      }
      
      laser_msgs.pop_front();
      
      if (save_clouds)
	mapper->saveData();

      int currSize = sgraph->graph()->vertices().size();
      if (currSize != prevSize || mapper->forceDisplay()) {
	// Only update map view if changed
	
	if (publish_map) {
	  //ROS map publisher
	  for (OptimizableGraph::VertexIDMap::iterator it=sgraph->graph()->vertices().begin(); it!=sgraph->graph()->vertices().end(); ++it) {
	    VertexSE2* v = (VertexSE2*) (it->second);
	    sgraph->addScanFromCloud(v);
	  }
	  graph2occ->compute();
	  gserver->publish();
	}

	if (publish_graph)
	  gpub->publishGraph();

	if (use_gui)
	  sviewer->updateGL();

	prevSize = currSize;
      }

      if (use_gui) {
	tviewer->updateGL();

	//Processing events
	QKeyEvent* event=tviewer->lastKeyEvent();
	if (event) {
	  if (event->key()==Qt::Key_F) {
	    tviewer->setFollowRobotEnabled(!tviewer->followRobotEnabled());
	    tviewer->keyEventProcessed();
	  }
	  if (event->key()==Qt::Key_B) {
	    tviewer->setDrawRobotEnabled(!tviewer->drawRobotEnabled());
	    tviewer->keyEventProcessed();
	  }
	  if (event->key()==Qt::Key_R) {
	    tracker->reset();
	    tviewer->keyEventProcessed();
	  }
	  if (event->key()==Qt::Key_H) {
	    cerr << "HELP" << endl;
	    cerr << "R: reset tracker" << endl;
	    cerr << "B: draws robot" << endl;
	    cerr << "F: enables/disables robot following" << endl;
	    tviewer->keyEventProcessed();
	  }
	  if (event->key()==Qt::Key_O) {//SAVE
	    tviewer->keyEventProcessed();
	    sgraph->saveGraph(outgraphFilename.c_str());
	    sgraph->exportTrajectory("trajectory.g2o");
	    //sgraph->exportTrajectoryOriginalLaser("trajectoryLaser.g2o");
	    cerr << "Map saved." << endl;
	  }
	}
      }
    }
    loop_rate.sleep();
    ros::spinOnce();
  }

  std::cerr << "Finish mapping ... " << std::endl;

  //Managing last information added
  mapper->finishMapping();
  
  std::cerr << "Saving graph... " << outgraphFilename << std::endl;
  sgraph->saveGraph(outgraphFilename.c_str());
  if (save_clouds){
    sgraph->saveClouds();
  }
  sgraph->exportTrajectory("trajectory.g2o");
  sgraph->exportStampedTrajectory("stamped_trajectory.g2o");
  sgraph->exportTrajectoryOriginalLaser("trajectoryLaser.g2o");

  std::cerr << "Finished." << std::endl;

  if (publish_tf)
    gpub->stop();
  
  if (use_gui){
    app->exec();
    delete app;
  }
  
}
