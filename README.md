# srrg_mapper2d_ros

This package implements a 2D graph SLAM approach based on local maps of 2D clouds.

## Prerequisites

This package requires:
* [srrg_cmake_modules](https://gitlab.com/srrg-software/srrg_cmake_modules)
* [srrg_core](https://gitlab.com/srrg-software/srrg_core)
* [srrg_core_viewers](https://gitlab.com/srrg-software/srrg_core_viewers)
* [srrg_gl_helpers](https://gitlab.com/srrg-software/srrg_gl_helpers)
* [srrg_scan_matcher](https://gitlab.com/srrg-software/srrg_scan_matcher)
* [srrg_scan_matcher_gui](https://gitlab.com/srrg-software/srrg_scan_matcher_gui)
* [srrg_mapper2d](https://gitlab.com/srrg-software/srrg_mapper2d)
* [srrg_mapper2d_gui](https://gitlab.com/srrg-software/srrg_mapper2d_gui)

This package depends on **g2o**:
* [g2o](https://github.com/RainerKuemmerle/g2o)

## Installation

Clone and compile the above mentioned packages on your catkin workspace.
For a clean installation of the `srrg` environment check [srrg_scripts](https://gitlab.com/srrg-software/srrg_scripts) using as option `packages_mapper2d.txt` during the setup.

## Applications

* `srrg_mapper2d_node`:
You can run the ROS node by typing:

```sh
$ rosrun srrg_mapper2d_ros srrg_mapper2d_node
```

The output of this process is exported into a map in g2o format that can be converted into an occupancy grid compatible with the ROS format using the `srrg_g2o2ros` node available in this package.

## References
Please cite the following paper to reference our work.

```
   @INPROCEEDINGS{LazaroIROS18,
     author = {M. T. L\'azaro and R. Capobianco  and G. Grisetti},
     title = {Efficient Long-term Mapping in Dynamic Environments},
     booktitle = {IEEE/RSJ International Conference on Intelligent Robots and Systems},
     month = {Oct 1-5},
     year = {2018},
     address = {Madrid, Spain}
   } 
```
